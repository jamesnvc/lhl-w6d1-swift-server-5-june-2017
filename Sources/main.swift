import Vapor

let drop = Droplet()

drop.get("/hello") { request in
    return "Hello world"
}

// In-memory array; in reality this would be a database
// exercise for reader: use an actual database
var names = [String]()

drop.post("/users") { request in
    if let name = request.data["name"]?.string {
        names.append(name)
        return "ok"
    } else {
        return Response(status: .error, body: "Missing name param")
    }
}

drop.get("/users") { _ in
// exercise for reader: send this back as JSON
// and set content-type appropriately
    return "\(names)"
}

drop.delete("/users" { request in
    if let toRemove = request.data["name"]?.string {
        // remove toRemove from names
    } else {
        // return error code
    }


    guard let toRemove = ... else { /* return error code */ }
    // remove toRemove from names
}

drop.run()
